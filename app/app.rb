require 'sinatra'
require 'sinatra/reloader'
require 'mysql2'
require 'json'

Mysqlclient = Mysql2::Client.new(host: 'db', username: 'root', password: 'toor', database: 'Prueba')

get '/' do
   data_mysql = Mysqlclient.query('SELECT * FROM empleados;', :as => :hash).to_a
   p data_mysql.to_json
end

get '/list/' do
  erb :list, locals: { data: querys}
end

get '/example/:id' do
  query = querys[params[:id].to_i]
  data = Mysqlclient.query(query[:query], :as => :hash)
  erb :table, locals: {question: query[:question],title: query[:query], data: data }
end

def querys
  [
    {question: "SIMPLE - Empleado", query: "SELECT * FROM empleados;"},
    {question: "SIMPLE - Oficinas", query: "SELECT * FROM oficinas;"},
    {question: "SIMPLE - Productos", query: "SELECT * FROM productos;"},
    {question: "SIMPLE - Pedidos", query: "SELECT * FROM pedidos;"},
    {question: "SIMPLE - Clientes", query: "SELECT * FROM clientes;"},
    {question: "SIMPLE - Listar de cada empleado su código, nombre, oficina, y fecha de contrato (fecha corta).", query: 'SELECT numemp, nombre, oficina, DATE_FORMAT(contrato, "%d-%m-%Y") AS `Fecha contratos` FROM empleados;'},
    {question: "SIMPLE - Listar de cada pedido, su número, código completo del artículo vendido y precio unitario al que se ha vendido.", query: "SELECT numpedido, fab, producto, importe, cant, importe/cant AS Precio FROM pedidos;"},
    {question: "SIMPLE - Lo mismo que la anterior pero con precios redondeados.", query: "SELECT numpedido, fab, producto, importe, cant, round(importe/cant,2) AS Precio FROM pedidos;"},
    {question: "SIMPLE - Obtener una lista de productos por fabricante.", query: "SELECT idfab, descripcion, idproducto, productos.precio FROM productos ORDER BY idfab, descripcion;"},
    {question: "SIMPLE - De cada producto queremos saber el id de fabricante, id de producto, su descripción y el valor de sus existencias.", query: "SELECT idfab,idproducto,descripcion,(existencias*precio) AS valoracion FROM productos;"},
    {question: "SIMPLE - Listar los empleados indicando código, nombre y día de la semana en que fue contratado.", query: "SELECT numemp, nombre, DAYNAME(contrato) AS `Día de contrato` FROM empleados;"},
    {question: "SIMPLE - Listar de cada empleado su código, nombre y título (sustituir la abreviatura dir por la palabra completa Director).", query: "SELECT numemp, nombre, REPLACE(titulo, 'dir ', 'Director ') AS Cargo FROM empleados;"},
    {question: "SIMPLE - Listar los datos de los empleados que no tienen oficina asignada", query: "SELECT DISTINCT oficina, numemp, nombre FROM empleados WHERE oficina IS NOT NULL;"},
    {question: "SIMPLE - Listar las oficinas de manera que las oficinas de mayores ventas aparezcan en primer lugar.", query: "SELECT ciudad, region, ventas FROM oficinas ORDER BY ventas DESC;"},
    {question: "", query: ""},
    {question: "MULTI -  Listar los códigos y nombres de los empleados de las oficinas del Este con su oficina y ciudad.", query: "SELECT numemp, nombre, empleados.oficina, ciudad FROM oficinas INNER JOIN empleados ON oficinas.oficina = empleados.oficina WHERE region ='Este';"},
    {question: "MULTI - Listar todos los pedidos mostrando su número, importe, nombre de cliente, y el límite de crédito del cliente correspondiente. ", query: "SELECT numpedido, importe, clientes.nombre AS Cliente, limitecredito FROM pedidos INNER JOIN clientes ON clie=numclie;"},
    {question: "MULTI -  Listar todos los empleados y la ciudad y región donde trabaja.  ", query: 'SELECT numemp, nombre, edad, titulo, DATE_FORMAT(contrato, "%d-%m-%Y") AS contrato, jefe, empleados.oficina,ciudad, region
FROM oficinas RIGHT JOIN empleados ON oficinas.oficina = empleados.oficina;'},
    {question: "MULTI -  Listar todas las oficinas y los nombres y títulos de sus directores.", query: "SELECT oficinas.*, nombre AS director, titulo FROM oficinas LEFT JOIN empleados ON dir = numemp;"},
    {question: "MULTI - Listar las oficinas con objetivo superior a 60.000 euros indicando para cada una el nombre de su director. ", query: "SELECT oficinas.*, nombre AS director FROM oficinas LEFT JOIN empleados ON dir = numemp WHERE objetivo > 60000;"},
    {question: "MULTI - Listar los pedidos superiores a 250 euros, incluyendo el nombre del vendedor que tomó el pedido y el nombre del cliente que lo solicitó. ", query: 'SELECT numpedido, DATE_FORMAT(fechapedido, "%d-%m-%Y") AS fechapedido, clie, rep, fab,
producto, cant, importe, clientes.nombre AS cliente, empleados.nombre AS vendedor FROM (pedidos INNER JOIN empleados ON rep = numemp) INNER JOIN clientes ON clie = numclie WHERE importe > 250;'},
    {question: "MULTI - Listar los pedidos superiores a 250 euros, mostrando el nombre del cliente que solicitó el pedido y el nombre del vendedor asignado a ese cliente. ", query: "SELECT pedidos.*, clientes.nombre AS cliente, empleados.nombre AS `vendedor asignado` FROM (pedidos INNER JOIN clientes ON clie = numclie) INNER JOIN empleados ON repclie = numemp WHERE importe > 250;"},
    {question: "MULTI - Listar los pedidos superiores a 250 euros, mostrando además el nombre del cliente que solicitó o el pedido y el nombre del vendedor asignado a ese cliente y la ciudad de la oficina donde el vendedor trabaja. ", query: "SELECT numpedido, clie, rep, clientes.nombre AS cliente, repclie, empleados.nombre AS vendedor, ciudad FROM ((pedidos INNER JOIN clientes ON clie = numclie) INNER JOIN empleados ON repclie = numemp) LEFT JOIN oficinas ON empleados.oficina=oficinas.oficina WHERE importe > 250;"},
    {question: "MULTI - Hallar los pedidos recibidos los días en que un nuevo empleado fue contratado. ", query: "SELECT numpedido, fechapedido, rep, numemp, nombre, contrato FROM pedidos, empleados WHERE fechapedido=contrato;"},
    {question: "MULTI - Hallar los empleados que realizaron su primer pedido el mismo día contratados. ", query: "SELECT numemp, nombre, contrato, numpedido, rep, fechapedido FROM pedidos INNER JOIN empleados ON rep = numemp WHERE fechapedido = contrato;"},
    {question: "MULTI - Listar los empleados que tienen una cuota superior al objetivo de al menos una oficina. La oficina puede ser cualquiera no tiene por que ser la del empleado. ", query: "SELECT numemp, nombre, cuota, empleados.oficina AS 'Su oficina', oficinas.oficina, objetivo FROM empleados, oficinas
WHERE cuota > objetivo"},
    {question: "MULTI - Listar de cada empleado su código, nombre, cuota, y código, nombre y cuota de su jefe. ", query: "SELECT empleados.numemp, empleados.nombre, empleados.cuota, empleados.jefe, jefes.nombre AS `Nombre jefe`, jefes.cuota AS `Cuota jefe` FROM empleados LEFT JOIN empleados jefes ON empleados.jefe = jefes.numemp;"},
    {question: "MULTI - Listar los empleados con una cuota superior a la de su jefe, los campos a obtener son el número, nombre y cuota del empleado y número, nombre y cuota de su jefe. ", query: 'SELECT empleados.numemp, empleados.nombre, empleados.cuota, empleados.jefe, jefes.nombre, jefes.cuota FROM empleados INNER JOIN empleados jefes ON empleados.jefe = jefes.numemp WHERE empleados.cuota > jefes.cuota;'},
    {question: "MULTI - Listar los empleados que no están asignados a la misma oficina que su jefe, queremos nñumero, nombre y número de oficina tanto del empleado como se su jefe. ", query: 'SELECT e.numemp, e.nombre, e.oficina, ofiemp.ciudad, e.jefe, j.nombre as `nombre jefe`, j.oficina AS `oficina jefe`, ofijefe.ciudad FROM (oficinas ofiemp RIGHT JOIN empleados e ON ofiemp.oficina= e.oficina) INNER JOIN (empleados j LEFT JOIN oficinas ofijefe ON j.oficina = ofijefe.oficina) ON e.jefe = j.numemp WHERE e.oficina <> j.oficina OR e.oficina IS NULL OR j.oficina IS NULL;'},
    {question: "", query: ''},
    {question: "RESUMEN - ¿Cuál es la cuota media y las ventas medias de los empleados? ", query: 'SELECT AVG(cuota) AS `Cuota media`, ROUND(AVG(ventas),2) AS `Ventas medias` FROM empleados;'},
    {question: "RESUMEN - ¿De media, cuánto superávit obtienen los empleados, diferencia entre lo vendido y su cuota? ", query: 'SELECT AVG(ventas-cuota) AS `Superávit medio` FROM empleados;'},
    {question: "RESUMEN - ¿Cuál es el total de cuotas y total de ventas de todos los empleados? ", query: 'SELECT SUM(cuota) AS `Total cuotas`, SUM(ventas) AS `Total ventas` FROM empleados;'},
    {question: "RESUMEN - ¿Cuántas oficinas tenemos en Valencia? ", query: 'SELECT COUNT(*) AS Valencianas FROM oficinas WHERE ciudad = "Valencia";'},
    {question: "RESUMEN - ¿Cuántas oficinas tienen empleados? ", query: 'SELECT COUNT(DISTINCT oficina) AS `Oficinas con empleados` FROM empleados;'},
    {question: "RESUMEN - ¿Cuántos empleados superan su cuota (tienen superávit positivo)?", query: 'SELECT count(*) AS `Empleados buenos` FROM empleados
WHERE ventas > cuota ;'},
    {question: "RESUMEN -¿ Cuál es el importe total de los pedidos realizados por el empleado Luis Antonio?", query: 'SELECT SUM(importe) AS `Importe vendido`
FROM pedidos INNER JOIN empleados ON rep = numemp WHERE nombre = "Luis Antonio";'},
    {question: "RESUMEN -Hallar el precio medio de los productos del fabricante aci.", query: 'SELECT AVG(precio) AS `precio medio` FROM productos WHERE idfab = "aci";'},
    {question: "RESUMEN -Calcular el importe medio de los pedidos realizados por el cliente 2103.", query: 'SELECT AVG(importe) AS `importe medio` FROM pedidos
WHERE clie = 2103;'},
    {question: "RESUMEN - Hallar el mejor superávit de todos los empleados.", query: 'SELECT MAX(ventas - cuota) AS `Mejor superavit` FROM empleados;'},
    {question: "RESUMEN - Calcular el importe medio de los pedidos realizados por el cliente 2103.", query: 'SELECT AVG(importe) AS `importe medio` FROM pedidos WHERE clie = 2103;'},
    {question: "RESUMEN - ¿Entre qué cuotas se mueven los empleados?", query: 'SELECT MIN(cuota) AS `Cuota mínima`, MAX(cuota) AS `Cuota máxima` FROM empleados;'},
    {question: "RESUMEN - ¿Cuántas oficinas tienen empleados cuyas ventas superan el objetivo de su oficina?", query: 'SELECT COUNT(DISTINCT empleados.oficina) AS Cuántas FROM empleados INNER JOIN oficinas ON empleados.oficina = oficinas.oficina WHERE empleados.ventas > objetivo;'},
    {question: "RESUMEN - Para cada empleado cuyos pedidos suman más de 300 euros, hallar el importe medio vendido.", query: 'SELECT rep, AVG(importe) AS `Importe medio` FROM pedidos GROUP BY rep HAVING SUM(importe) > 300;'},
    {question: "RESUMEN - Rectifica la consulta anterior para que aparezca también el nombre del empleado.", query: 'SELECT rep, nombre, AVG(importe) AS `Importe medio` FROM empleados INNER JOIN pedidos ON numemp = rep GROUP BY rep, nombre HAVING SUM(importe) > 300;'},
    {question: "RESUMEN -De cada empleado, obtener el importe vendido a cada cliente.", query: 'SELECT rep, clie AS Cliente, SUM(importe) AS `Importe vendido`
FROM pedidos GROUP BY rep, clie;'},
    {question: "RESUMEN -Repetir la consulta anterior pero ahora deben aparecer también los empleados
que no han vendido nada.", query: 'SELECT numemp, clie, SUM(importe) AS `Importe vendido` FROM empleados LEFT JOIN pedidos ON numemp = rep GROUP BY numemp, clie ORDER BY numemp,clie;'},
    {question: "RESUMEN -Repetir la consulta pero ahora debe aparecer también el total de cuánto ha vendido cada empleado.", query: 'SELECT numemp, clie, SUM(importe) AS `Importe vendido` FROM empleados LEFT JOIN pedidos ON numemp = rep GROUP BY numemp, clie WITH ROLLUP'},
    {question: "", query: ""},
    {question: "SUBCONSULTAS - Listar los numclie y nombres de los clientes que tienen asignado el representante Juan Gris.", query: "SELECT numclie, nombre FROM clientes WHERE repclie = ANY (SELECT numemp FROM empleados WHERE nombre = 'Juan Gris');"},
    {question: "SUBCONSULTAS - Listar todos los productos (código, descripción y stock) del fabricante ACI y cuyas existencias superan las existencias del producto ACI-41004.", query: "SELECT idfab, idproducto, descripcion, existencias FROM productos WHERE idfab = 'ACI' AND existencias > (SELECT existencias FROM productos WHERE idfab = 'ACI' AND idproducto = '41004');"},
    {question: "SUBCONSULTAS - Listar los empleados (numemp, nombre y oficina) que no trabajan en oficinas dirigidas por el empleado 108.", query: "SELECT numemp, nombre, oficina FROM empleados WHERE NOT EXISTS (SELECT * FROM oficinas WHERE dir = 108 and empleados.oficina = oficinas.oficina);"},
    {question: "SUBCONSULTAS - Listar los empleados (numemp, nombre y edad) que no dirigen una oficina.", query: "SELECT numemp, nombre, edad FROM empleados WHERE NOT EXISTS (SELECT * FROM oficinas WHERE dir = numemp)"},
    {question: "SUBCONSULTAS - Listar los empleados (numemp, nombre) mayores de 40 años que dirigen a un vendedor con superávit (ha vendido más que su cuota).", query: "SELECT numemp, nombre FROM empleados WHERE edad > 40 and numemp IN (SELECT jefe FROM empleados WHERE ventas > cuota )"},
    {question: "SUBCONSULTAS - Listar las oficinas en donde todos los empleados tienen ventas que superan al 50% del objetivo de la oficina.", query: "SELECT oficina, ciudad FROM oficinas WHERE (objetivo * 0.5) <= ( SELECT MIN(ventas) FROM empleados WHERE empleados.oficina = oficinas.oficina);"},

  ]
end
