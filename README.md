# Ejemplos de sentencias SQL en ruby-sinatra-mysql

Entorno docker de Ruby Mysql y el gestor grafico Adminitrator donde se cargar una base de datos de ejemplo y se corren una serie de sentencias sql sobre esta. 

## Puesta en marcha

Para que corran las pruebas tenemos primero que importar la base de datos

tenemos que arrancar los contenedores con

    docker-compose up

entramos con el navegador al gestor grafico de bases de datos

http://localhost:3001

    servidor: db
    Usuario: root
    contrase;a: toor

centro creamos una base de datos nueva llamada Prueba

e importamos el archivo DB-prueba.sql dentro de ella

correr las sql, en http://localhost:3000

- http://localhost:3000/ #ejemplo en json de la tabla pedidos
- http://localhost:3000/list/  # list de sentencia

la base de dato sobre las que corren los ejemplos tienen esta estructura

![tablas oficinas-empleado](oficinas-empleado.png)

![tablas cliente-pedidos-productos](cliente-pedidos-productos.png)

## contenedor mysql

entrar en el contenerdor de mysql

       docker exec -it stra_mysql bash

llamar la cliente de mysql

       mysql --user=root --password

### algo que hacer dentro

       show databases;

       create nuevabase;

       use nuevabase;

       create nuevatabla (
         id INT AUTO_INCREMENT,
         name VARCHAR(255) NOT NULL,
         PRIMARY KEY (id)
         )  ENGINE=INNODB;

       show tables;

       drop table nuevatabla;

       drop database nuevabase;
