-- Adminer 4.7.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `numclie` int(11) DEFAULT NULL,
  `nombre` varchar(17) DEFAULT NULL,
  `repclie` int(11) DEFAULT NULL,
  `limitecredito` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `clientes` (`numclie`, `nombre`, `repclie`, `limitecredito`) VALUES
(2101,	'Luis García Antón',	106,	6500),
(2102,	'Alvaro Rodríguez',	101,	6500),
(2103,	'Jaime Llorens',	110,	5000),
(2105,	'Antonio Canales',	101,	4500),
(2106,	'Juan Suárez',	102,	650),
(2107,	'Julian López',	110,	3500),
(2108,	'Julia Antequera',	109,	550),
(2109,	'Alberto Juanes',	103,	250),
(2111,	'Cristóbal García',	103,	500),
(2112,	'María Silva',	108,	5000),
(2113,	'Luisa Maron',	104,	2000),
(2114,	'Cristina Bulini',	102,	2000),
(2115,	'Vicente Martínez',	101,	2000),
(2117,	'Carlos Tena',	106,	3500),
(2118,	'Junípero Alvarez',	108,	600),
(2119,	'Salomon Bueno',	109,	2500),
(2120,	'Juan Malo',	102,	5000),
(2121,	'Vicente Ríos',	103,	4500),
(2122,	'José Marchante',	105,	3000),
(2123,	'José Libros',	102,	400),
(2124,	'Juan Bolto',	107,	4000),
(2125,	'Pepito Grillo',	111,	0);

DROP TABLE IF EXISTS `empleados`;
CREATE TABLE `empleados` (
  `numemp` int(11) DEFAULT NULL,
  `nombre` varchar(16) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `oficina` varchar(2) DEFAULT NULL,
  `titulo` varchar(16) DEFAULT NULL,
  `contrato` varchar(19) DEFAULT NULL,
  `jefe` varchar(3) DEFAULT NULL,
  `cuota` float DEFAULT NULL,
  `ventas` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `empleados` (`numemp`, `nombre`, `edad`, `oficina`, `titulo`, `contrato`, `jefe`, `cuota`, `ventas`) VALUES
(101,	'Antonio Viguer',	45,	'12',	'representante',	'1986-10-20 00:00:00',	'104',	30000,	30500),
(102,	'Alvaro Jaumes',	48,	'21',	'representante',	'1986-12-10 00:00:00',	'108',	35000,	47400),
(103,	'Juan Rovira',	29,	'12',	'representante',	'1987-03-01 00:00:00',	'104',	27500,	28600),
(104,	'José González',	33,	'12',	'dir ventas',	'1987-05-19 00:00:00',	'106',	20000,	14300),
(105,	'Vicente Pantalla',	37,	'13',	'representante',	'1988-02-12 00:00:00',	'104',	35000,	36800),
(106,	'Luis Antonio',	52,	'11',	'director general',	'1988-06-14 00:00:00',	NULL,	27500,	29900),
(107,	'Jorge Gutiérrez',	49,	'22',	'representante',	'1988-11-14 00:00:00',	'108',	30000,	18600),
(108,	'Ana Bustamante',	62,	'21',	'dir ventas',	'1989-10-12 00:00:00',	'106',	35000,	36100),
(109,	'María Sunta',	31,	NULL,	'representante',	'1999-10-12 00:00:00',	'106',	3000,	39200),
(110,	'Juan Victor',	41,	NULL,	'representante',	'1990-01-13 00:00:00',	'104',	0,	7600),
(111,	'Juan Gris',	50,	NULL,	'representante',	'2005-05-01 00:00:00',	NULL,	10000,	60000),
(112,	'Julián Martorell',	50,	NULL,	'representante',	'2006-05-01 00:00:00',	NULL,	10000,	91000),
(113,	'Juan Gris',	18,	NULL,	'representante',	'2007-01-01 00:00:00',	NULL,	10000,	0);

DROP TABLE IF EXISTS `oficinas`;
CREATE TABLE `oficinas` (
  `oficina` int(11) DEFAULT NULL,
  `ciudad` varchar(9) DEFAULT NULL,
  `region` varchar(6) DEFAULT NULL,
  `dir` varchar(3) DEFAULT NULL,
  `objetivo` float DEFAULT NULL,
  `ventas` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `oficinas` (`oficina`, `ciudad`, `region`, `dir`, `objetivo`, `ventas`) VALUES
(11,	'Valencia',	'este',	'106',	57500,	69300),
(12,	'Alicante',	'este',	'104',	80000,	73500),
(13,	'Castellon',	'este',	'105',	35000,	36800),
(21,	'Badajoz',	'oeste',	'108',	72500,	84400),
(22,	'A Coruña',	'oeste',	'108',	30000,	18600),
(23,	'Madrid',	'centro',	'108',	0,	0),
(24,	'Aranjuez',	'centro',	'108',	25000,	15000),
(25,	'Valencia',	NULL,	'',	0,	0),
(26,	'Pamplona',	'norte',	'',	0,	200000),
(27,	'Móstoles',	'Centro',	'',	0,	0),
(28,	'Valencia',	'este',	'',	90000,	0),
(29,	'Valencia',	'este',	'',	10000,	2100),
(30,	'pamplona',	'norte',	'',	20000,	0),
(31,	'Elx',	'',	'',	0,	0);

DROP TABLE IF EXISTS `pedidos`;
CREATE TABLE `pedidos` (
  `codigo` int(11) DEFAULT NULL,
  `numpedido` int(11) DEFAULT NULL,
  `fechapedido` varchar(19) DEFAULT NULL,
  `clie` int(11) DEFAULT NULL,
  `rep` int(11) DEFAULT NULL,
  `fab` varchar(3) DEFAULT NULL,
  `producto` varchar(5) DEFAULT NULL,
  `cant` int(11) DEFAULT NULL,
  `importe` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `pedidos` (`codigo`, `numpedido`, `fechapedido`, `clie`, `rep`, `fab`, `producto`, `cant`, `importe`) VALUES
(1,	110036,	'1989-10-12 00:00:00',	2107,	110,	'aci',	'4100z',	9,	22.5),
(2,	110037,	'1989-10-12 00:00:00',	2117,	106,	'rei',	'2a44l',	7,	31.5),
(3,	112963,	'2008-05-10 00:00:00',	2103,	105,	'aci',	'41004',	28,	3.276),
(4,	112968,	'1990-01-11 00:00:00',	2102,	101,	'aci',	'41004',	34,	39.78),
(5,	112975,	'2008-02-11 00:00:00',	2111,	103,	'rei',	'2a44g',	6,	21),
(6,	112979,	'1989-10-12 00:00:00',	2114,	108,	'aci',	'4100z',	6,	150),
(7,	112983,	'2008-05-10 00:00:00',	2103,	105,	'aci',	'41004',	6,	7.02),
(8,	112987,	'2008-01-01 00:00:00',	2103,	105,	'aci',	'4100y',	11,	275),
(9,	112989,	'2008-12-10 00:00:00',	2101,	106,	'fea',	'114',	6,	14.58),
(10,	112992,	'1990-04-15 00:00:00',	2118,	108,	'aci',	'41002',	10,	7.6),
(11,	112993,	'2008-03-10 00:00:00',	2106,	102,	'rei',	'2a45c',	24,	18.96),
(12,	112997,	'2008-04-04 00:00:00',	2124,	107,	'bic',	'41003',	1,	6.52),
(13,	113003,	'2008-02-05 00:00:00',	2108,	109,	'imm',	'779c',	3,	56.25),
(14,	113007,	'2008-01-01 00:00:00',	2112,	108,	'imm',	'773c',	3,	29.25),
(15,	113012,	'2008-05-05 00:00:00',	2111,	105,	'aci',	'41003',	35,	37.45),
(16,	113013,	'2008-12-28 00:00:00',	2118,	108,	'bic',	'41003',	1,	6.52),
(17,	113024,	'2008-07-04 00:00:00',	2114,	108,	'qsa',	'xk47',	20,	71),
(18,	113027,	'2008-02-05 00:00:00',	2103,	105,	'aci',	'41002',	54,	450),
(19,	113034,	'2008-11-05 00:00:00',	2107,	110,	'rei',	'2a45c',	8,	6.32),
(20,	113042,	'2008-01-01 00:00:00',	2113,	101,	'rei',	'2a44r',	5,	225),
(21,	113045,	'2008-07-02 00:00:00',	2112,	110,	'rei',	'2a44r',	10,	450),
(22,	113048,	'2008-02-02 00:00:00',	2120,	102,	'imm',	'779c',	2,	37.5),
(23,	113049,	'2008-04-04 00:00:00',	2118,	108,	'qsa',	'xk47',	2,	7.76),
(24,	113051,	'2008-07-06 00:00:00',	2118,	108,	'qsa',	'xk47',	4,	14.2),
(25,	113055,	'2009-04-01 00:00:00',	2108,	101,	'aci',	'4100x',	6,	1.5),
(26,	113057,	'2008-11-01 00:00:00',	2111,	103,	'aci',	'4100x',	24,	0),
(27,	113058,	'1989-07-04 00:00:00',	2108,	109,	'fea',	'112',	10,	14.8),
(28,	113062,	'2008-07-04 00:00:00',	2124,	107,	'bic',	'41003',	10,	24.3),
(29,	113065,	'2008-06-03 00:00:00',	2106,	102,	'qsa',	'xk47',	6,	21.3),
(30,	113069,	'2008-08-01 00:00:00',	2109,	107,	'imm',	'773c',	22,	313.5);

DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `idfab` varchar(3) DEFAULT NULL,
  `idproducto` varchar(5) DEFAULT NULL,
  `descripcion` varchar(11) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `existencias` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `productos` (`idfab`, `idproducto`, `descripcion`, `precio`, `existencias`) VALUES
('aci',	'41001',	'arandela',	1.2,	277),
('aci',	'41002',	'bisagra',	2.8,	167),
('aci',	'41003',	'art t3',	7,	207),
('aci',	'41004',	'art t4',	12,	139),
('aci',	'4100x',	'junta',	2.2,	37),
('aci',	'4100y',	'extractor',	2.4,	25),
('aci',	'4100z',	'mont',	3,	28),
('bic',	'41003',	'manivela',	6.52,	3),
('bic',	'41089',	'rodamiento',	2.25,	78),
('bic',	'41672',	'plato',	1.8,	0),
('fea',	'112',	'cubo',	1.48,	115),
('fea',	'114',	'cubo',	2.43,	15),
('imm',	'773c',	'reostato',	9.75,	28),
('imm',	'775c',	'reostato 2',	14.25,	5),
('imm',	'779c',	'reostato 3',	18.75,	0),
('imm',	'887h',	'caja clavos',	0.54,	223),
('imm',	'887p',	'perno',	0.25,	24),
('imm',	'887x',	'manivela',	4.75,	32),
('qsa',	'xk47',	'red',	3.55,	38),
('qsa',	'xk48',	'red',	1.34,	203),
('qsa',	'xk48a',	'red',	1.48,	37),
('rei',	'2a44g',	'pas',	3.5,	14),
('rei',	'2a44l',	'bomba l',	45,	12),
('rei',	'2a44r',	'bomba r',	45,	12),
('rei',	'2a45c',	'junta',	0.79,	210);

-- 2019-01-11 09:23:20
